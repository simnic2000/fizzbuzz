import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest {
    @Test
    public void feature1 () {
        Fizzbuzz fizzbuzz = new Fizzbuzz(1);
        assertEquals("1", fizzbuzz.Gioca());
    }

    @Test
    public void feature3 () {
        Fizzbuzz fizzbuzz = new Fizzbuzz(3);
        assertEquals("1 2 fizz", fizzbuzz.Gioca());
    }

    @Test
    public void feature5 () {
        Fizzbuzz fizzbuzz = new Fizzbuzz(5);
        assertEquals("1 2 fizz 4 buzz", fizzbuzz.Gioca());
    }

    @Test
    public void feature15 () {
        Fizzbuzz fizzbuzz = new Fizzbuzz(15);
        assertEquals("1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz", fizzbuzz.Gioca());
    }
}
